# Load Balancer

A load balancer is a component that, once
invoked, it distributes incoming requests to a list of
registered providers and return the value obtained
from one of the registered providers to the original
caller. For simplicity we will consider both the load
balancer and the provider having a public method
named get().

## Generate provider

*Generate a Provider that, once invoked on his
get() method, retrieve an unique identifier
(string) of the provider instance*

I created the Provider class inside the core package in the source of this code.

## Register a list of providers

*Register a list of provider instances to the
Load Balancer - the maximum number of
providers accepted from the load balancer is
10*

I created the Load Balancer class with the list of providers instances inside the core package in the source of this code.

## Random invocation

*Develop an algorithm that, when invoking multiple
times the Load Balancer on its get() method,
should cause the random invocation of the get()
method of any registered provider instance.*

I created the get method that get the provider randomly.

## Round Robin invocation

*Develop an algorithm that, when invoking multiple
times the Load Balancer on its get() method,
should cause the round-robin (sequential)
invocation of the get() method of the registered
providers.*

I created an other class LoadBalancerRoundRobin that extends LoadBalancer class and override the get method.
The idea is creating the round robin algorithm, that gets the provider sequentially.

## Manual node exclusion / inclusion

*Develop the possibility to exclude / include a
specific provider into the balancer*

I created the add and delete Provider methods in the LoadBalancer class.

## Heart beat checker

*The load balancer should invoke every X seconds
each of its registered providers on a special
method called check() to discover if they are alive
– if not, it should exclude the provider node from
load balancing.*

I created the HeartBeat class that implements Job that it will be called every x seconds.
Creation and configuration of the scheduler is made by HeartBeatScheduler class. In this class has a constant called 
INTERVAL_IN_SECONDS responsible for set the frequency that the HeartBeat class will be invoked.


## Improving Heart beat checker

*If a node has been previously excluded from the
balancing it should be re-included if it has
successfully been “heartbeat checked” for 2
consecutive times*

I devepelod a method in the HeartBeat class that re-include the provider that was removed if was checked for 2 consecutive times.

## Cluster Capacity Limit

*Assuming that each provider can handle a
maximum number of Y parallel requests, the
Balancer should not accept any further request
when it has (Y x aliveproviders) incoming requests
running simultaneously*

For calculate the number of parallel requests I used the concept of a bucket that accepts tokens until the bucket is full.

The bucket would be this collection to process in a concurrent way:
```
protected final ConcurrentLinkedQueue<UUID> bucketRequests = new ConcurrentLinkedQueue<>();
```

The idea is in the beginning of the method add a UUID to the bucket. After process everything, remove the UUID from the bucket.
After add the UUID, we need to check if the bucket is full (Y*aliveproviders).

In the main class I simulate if it's working correctly. I created 1000 threads, simulating that each thread is 
calling the get method in the LoadBalancer.

```java
for (int i = 0; i < 1000; i++) {
    log.info("Creating job " + i);
    new ThreadTest("Thread" + i, loadBalancer);
}
```

We can see in the log messages about the bucket is full and the most of threads calls was rejected.

