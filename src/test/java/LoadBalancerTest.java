import core.LoadBalancer;
import core.Provider;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


@Slf4j
public class LoadBalancerTest {

	@Test
	public void addProvider_listEmptyAndValidProvider_returnProviderAdded() {

		LoadBalancer loadBalancer = new LoadBalancer();

		var newProvider = new Provider();

		var result = loadBalancer.addProvider(newProvider);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(newProvider.getIdentifier(), result.getIdentifier());
			Assertions.assertEquals(newProvider.getIdentifier(), loadBalancer.providers.get(0).get());
		});

	}

	@Test
	public void addProvider_listSizeEqualsMaxProvider_providerNotAddedInTheList() {

		LoadBalancer loadBalancer = new LoadBalancer();
		for (int i = 0; i < 10; i++) {
			loadBalancer.addProvider(new Provider());
		}

		var newProvider = new Provider();

		var result = loadBalancer.addProvider(newProvider);

		Assertions.assertAll(() -> {
			Assertions.assertNull(result);
			Assertions.assertEquals(10, loadBalancer.providers.size());
		});

	}

	@Test
	public void addProvider_providerExistsInTheList_providerNotAddedInTheList() {

		LoadBalancer loadBalancer = new LoadBalancer();
		var provider = new Provider();

		loadBalancer.addProvider(provider);

		var result = loadBalancer.addProvider(provider);

		Assertions.assertAll(() -> {
			Assertions.assertNull(result);
			Assertions.assertEquals(1, loadBalancer.providers.size());
		});
	}

	@Test
	public void deleteProvider_providerExistsInTheList_providerDeleted() {

		LoadBalancer loadBalancer = new LoadBalancer();
		var provider = new Provider();

		loadBalancer.addProvider(provider);

		var result = loadBalancer.deleteProvider(provider);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(result, provider);
			Assertions.assertTrue(loadBalancer.providers.isEmpty());
		});
	}

	@Test
	public void deleteProvider_providerNotExistInTheList_providerNotDeleted() {

		LoadBalancer loadBalancer = new LoadBalancer();
		var provider = new Provider();

		loadBalancer.addProvider(provider);

		var result = loadBalancer.deleteProvider(new Provider());

		Assertions.assertAll(() -> {
			Assertions.assertNull(result);
			Assertions.assertEquals(1, loadBalancer.providers.size());
		});
	}

}
