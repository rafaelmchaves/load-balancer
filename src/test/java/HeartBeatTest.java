import core.HeartBeat;
import core.LoadBalancer;
import core.LoadBalancerRoundRobin;
import core.Provider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HeartBeatTest {

	private final LoadBalancer loadBalancerRoundRobin = LoadBalancerRoundRobin.getINSTANCE();

	@BeforeEach
	public void setup() {
		loadBalancerRoundRobin.removedProviders.clear();
		loadBalancerRoundRobin.providers.clear();
	}

	@Test
	public void checkAliveProviders_providerCheckedTrue_providerIsNotRemoved() {

		var newProvider = new Provider();
		loadBalancerRoundRobin.addProvider(newProvider);

		HeartBeat heartBeat = new HeartBeat();
		heartBeat.execute(null);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(1, loadBalancerRoundRobin.providers.size());
			Assertions.assertEquals(newProvider.getIdentifier(), loadBalancerRoundRobin.providers.get(0).getIdentifier());
			Assertions.assertTrue(loadBalancerRoundRobin.removedProviders.isEmpty());
		});

	}

	@Test
	public void checkAliveProviders_providerCheckedFalse_providerIsRemoved() {

		var newProvider = new Provider();
		newProvider.setAlive(false);
		loadBalancerRoundRobin.addProvider(newProvider);

		HeartBeat heartBeat = new HeartBeat();
		heartBeat.execute(null);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(0, loadBalancerRoundRobin.providers.size());
			Assertions.assertEquals(1, loadBalancerRoundRobin.removedProviders.size());
			Assertions.assertEquals(newProvider.getIdentifier(), loadBalancerRoundRobin.removedProviders.get(0).getIdentifier());
		});

	}

	@Test
	public void checkRemovedProviders_providerCheckedTrueOneTime_providerIsNotAdded() {

		var removedProvider = new Provider();
		removedProvider.setAlive(false);
		loadBalancerRoundRobin.addProvider(removedProvider);

		HeartBeat heartBeat = new HeartBeat();
		heartBeat.execute(null);
		removedProvider.setAlive(true);

		//when
		heartBeat.execute(null);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(0, loadBalancerRoundRobin.providers.size());
			Assertions.assertEquals(1, loadBalancerRoundRobin.removedProviders.size());
			Assertions.assertEquals(removedProvider.getIdentifier(), loadBalancerRoundRobin.removedProviders.get(0).getIdentifier());
			Assertions.assertEquals(1, loadBalancerRoundRobin.removedProviders.get(0).getCheckerAliveCount().get());
		});

	}

	@Test
	public void checkRemovedProviders_providerCheckedTrueTwoTimes_addProviderInTheList() {

		var removedProvider = new Provider();
		removedProvider.setAlive(false);
		loadBalancerRoundRobin.addProvider(removedProvider);

		HeartBeat heartBeat = new HeartBeat();
		heartBeat.execute(null);
		removedProvider.setAlive(true);

		//when
		heartBeat.execute(null);
		heartBeat.execute(null);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(1, loadBalancerRoundRobin.providers.size());
			Assertions.assertEquals(removedProvider.getIdentifier(), loadBalancerRoundRobin.providers.get(0).getIdentifier());
			Assertions.assertEquals(0, loadBalancerRoundRobin.removedProviders.size());
			Assertions.assertEquals(2, loadBalancerRoundRobin.providers.get(0).getCheckerAliveCount().get());
		});

	}

	@Test
	public void checkRemovedProviders_providerCheckedTrueOneTimeAndFalseOneTime_providerIsNotAdded() {

		var removedProvider = new Provider();
		removedProvider.setAlive(false);
		loadBalancerRoundRobin.addProvider(removedProvider);

		HeartBeat heartBeat = new HeartBeat();
		heartBeat.execute(null);
		removedProvider.setAlive(true);

		//when
		heartBeat.execute(null);
		removedProvider.setAlive(false);
		heartBeat.execute(null);

		Assertions.assertAll(() -> {
			Assertions.assertEquals(0, loadBalancerRoundRobin.providers.size());
			Assertions.assertEquals(1, loadBalancerRoundRobin.removedProviders.size());
			Assertions.assertEquals(removedProvider.getIdentifier(), loadBalancerRoundRobin.removedProviders.get(0).getIdentifier());
			Assertions.assertEquals(0, loadBalancerRoundRobin.removedProviders.get(0).getCheckerAliveCount().get());
		});
	}
}
