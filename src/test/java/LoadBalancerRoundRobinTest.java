import core.LoadBalancerRoundRobin;
import core.Provider;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@Slf4j
public class LoadBalancerRoundRobinTest {

	@Test
	public void get_twoProvidersInTheList_getProvidersSequentially() {
		LoadBalancerRoundRobin loadBalancerRoundRobin = new LoadBalancerRoundRobin();
		var provider1 = new Provider();
		var provider2 = new Provider();

		loadBalancerRoundRobin.addProvider(provider1);
		loadBalancerRoundRobin.addProvider(provider2);

		var request1 = loadBalancerRoundRobin.get();
		var request2 = loadBalancerRoundRobin.get();
		var request3 = loadBalancerRoundRobin.get();
		var request4 = loadBalancerRoundRobin.get();

		Assertions.assertAll(() -> {
			Assertions.assertEquals(provider1.getIdentifier(), request1);
			Assertions.assertEquals(provider2.getIdentifier(), request2);
			Assertions.assertEquals(provider1.getIdentifier(), request3);
			Assertions.assertEquals(provider2.getIdentifier(), request4);
		});
	}


}
