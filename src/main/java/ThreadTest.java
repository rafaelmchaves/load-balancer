import core.LoadBalancer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadTest implements Runnable{
	java.lang.Thread t;

	String name;

	LoadBalancer loadBalancer;

	public ThreadTest(String name, LoadBalancer loadBalancer) {
		this.loadBalancer = loadBalancer;
		this.name = name;
		this.t = new Thread(this, name);
		t.start();
	}

	@Override
	public void run() {
		try {
			for(int i = 5; i > 0; i--) {

				var provider = loadBalancer.get();
				log.info("thread name: {}, provider: {}", name, provider);
				Thread.sleep(1000);
			}
		}catch (InterruptedException e) {
			log.error(name + "Interrupted", e);
		}
		catch (Exception e) {
			log.error("Something was wrong to get a provider", e);
		}
		log.info(name + " exiting.");
	}
}

