package core;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class LoadBalancerRoundRobin extends LoadBalancer {

	public synchronized static LoadBalancer getINSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new LoadBalancerRoundRobin();
		}

		return INSTANCE;
	}

	private final AtomicInteger count = new AtomicInteger(0);

	@Override
	public String get() {

		bucketRequests.add(UUID.randomUUID());

		if (providers.isEmpty() || bucketRequests.size() >= getMaxParallelRequests()) {
			log.warn("Bucket full, size: {}", bucketRequests.size());
			bucketRequests.remove();
			return null;
		}

		var provider = getProvider();

		bucketRequests.remove();
		return provider;
	}

	private String getProvider() {
		var position = count.get();

		if (position >= providers.size()) {
			count.set(0);
		}

		position = count.getAndIncrement();

		return providers.get(position).get();
	}

	private Integer getMaxParallelRequests() {
		return MAX_PROVIDERS * REQUESTS_PER_PROVIDER;
	}

}
