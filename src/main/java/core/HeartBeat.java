package core;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class HeartBeat implements Job {

	private final LoadBalancer loadBalancerRoundRobin = LoadBalancerRoundRobin.getINSTANCE();

	@Override
	public void execute(JobExecutionContext jobExecutionContext) {

		checkRemovedProviders();

		checkAliveProviders();

	}

	/**
	 * Step 6 – Heart beat checker
	 * The load balancer should invoke every X seconds
	 * each of its registered providers on a special
	 * method called check() to discover if they are alive
	 * – if not, it should exclude the provider node from
	 * load balancing.
	 * */
	private void checkAliveProviders() {

		try {
			for (Provider provider: loadBalancerRoundRobin.providers) {

				var alive = provider.check();
				log.info("checkin the heatlh:" + provider.get() + "  alive:" + alive);

				if(!alive) {
					loadBalancerRoundRobin.deleteProvider(provider);
					provider.setCheckerAliveCount(new AtomicInteger(0));
					loadBalancerRoundRobin.removedProviders.add(provider);
				}
			}
		}  catch (Exception e) {
			log.error("Error to check alive providers: ", e);
		}

	}

	/**
	 * Step 7 – Improving Heart beat checker
	 * If a node has been previously excluded from the
	 * balancing it should be re-included if it has
	 * successfully been “heartbeat checked” for 2
	 * consecutive times
	* */
	private void checkRemovedProviders() {

		try {
			loadBalancerRoundRobin.removedProviders.forEach(remodedProvider -> {
				if (remodedProvider.check()) {
					remodedProvider.getCheckerAliveCount().incrementAndGet();
				} else {
					remodedProvider.getCheckerAliveCount().decrementAndGet();
				}

				if (remodedProvider.getCheckerAliveCount().get() >= 2) {
					loadBalancerRoundRobin.addProvider(remodedProvider);
					loadBalancerRoundRobin.removedProviders.remove(remodedProvider);
				}
			});
		} catch (Exception e) {
			log.error("Error to check removed providers: ", e);
		}
	}

}
