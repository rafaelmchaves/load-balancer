package core;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Provider {

	private final String identifier;

	private Boolean alive;

	private AtomicInteger checkerAliveCount;

	public AtomicInteger getCheckerAliveCount() {
		return checkerAliveCount;
	}

	public void setCheckerAliveCount(AtomicInteger checkerAliveCount) {
		this.checkerAliveCount = checkerAliveCount;
	}

	public Provider() {
		this.identifier = UUID.randomUUID().toString();
		alive = true;
	}

	public String get() {
		return identifier;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Provider provider = (Provider) o;
		return Objects.equals(identifier, provider.identifier);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	public Boolean check() {
		return alive;
	}

	public void setAlive(Boolean alive) {
		this.alive = alive;
	}

	public String getIdentifier() {
		return identifier;
	}
}
