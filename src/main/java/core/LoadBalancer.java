package core;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

public class LoadBalancer {

	protected static LoadBalancer INSTANCE;
	public final List<Provider> providers = new CopyOnWriteArrayList<>();

	public final List<Provider> removedProviders = new CopyOnWriteArrayList<>();

	protected final ConcurrentLinkedQueue<UUID> bucketRequests = new ConcurrentLinkedQueue<>();
	protected static final Integer MAX_PROVIDERS = 10;

	protected static final Integer REQUESTS_PER_PROVIDER = 10;

	public LoadBalancer() {

	}

	public synchronized static LoadBalancer getINSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new LoadBalancer();
		}

		return INSTANCE;
	}

	public String get() {
		Random r = new Random();
		var position = r.nextInt(providers.size());

		return providers.get(position).get();
	}

	public synchronized Provider addProvider(Provider provider) {

		if (providers.size() == MAX_PROVIDERS || providers.contains(provider)) {
			return null;
		}

		providers.add(provider);
		return provider;

	}

	public synchronized Provider deleteProvider(Provider provider) {

		if (!providers.contains(provider)) {
			return null;
		}

		providers.remove(provider);
		return provider;

	}

}
