package core;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class HeartBeatScheduler {

	private static final int INTERVAL_IN_SECONDS = 3;

	public static void createScheduler() throws SchedulerException {
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();
		Scheduler scheduler = schedulerFactory.getScheduler();
		JobDetail job = JobBuilder.newJob(HeartBeat.class)
			.withIdentity("heartBeatJob", "heartBeatJobGroup")
			.build();

		Trigger trigger = TriggerBuilder.newTrigger()
			.withIdentity("heartBeatTrigger", "heartBeatJobGroup")
			.startAt(DateBuilder.evenSecondDateAfterNow())
			.withSchedule(SimpleScheduleBuilder.simpleSchedule()
				.withIntervalInSeconds(INTERVAL_IN_SECONDS)
				.repeatForever())
			.build();

		scheduler.scheduleJob(job, trigger);
		scheduler.start();
	}
}
