import core.*;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

@Slf4j
public class Main {

	public static void main(String[] args) throws SchedulerException {

		HeartBeatScheduler.createScheduler();

		initializeLoadBalancerRoundRobin();
	}


	private static void initializeLoadBalancer() {
		var loadBalancer = LoadBalancer.getINSTANCE();

		System.out.println(loadBalancer.get());
		System.out.println(loadBalancer.get());
	}

	private static void initializeLoadBalancerRoundRobin() {

		var loadBalancer = LoadBalancerRoundRobin.getINSTANCE();
		loadBalancer.addProvider(new Provider());
		loadBalancer.addProvider(new Provider());
		loadBalancer.addProvider(new Provider());

		for (int i = 0; i < 1000; i++) {
			log.info("Creating thread " + i);
			new ThreadTest("Thread" + i, loadBalancer);
		}

		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			log.info("Main thread Interrupted");
		}
		log.info("Main thread exiting.");

	}

}

